<?php

namespace Tests\AppBundle\Service;

use AppBundle\Model\Circle;
use AppBundle\Model\GeometryShape;
use AppBundle\Model\Triangle;
use AppBundle\Service\GeometryCalculator;
use PHPUnit\Framework\TestCase;

class GeometryCalculatorTest extends TestCase
{
    /**
     * System under test.
     *
     * @var GeometryCalculator
     */
    private $geometryCalculator;

    public function setUp(): void
    {
        $this->geometryCalculator = new GeometryCalculator();
    }

    /** @test */
    public function surfaceSumForTwoGeometryShapesShouldReturnCorrectSumForCircles(): void
    {
        $a = new Circle(2);
        $b = new Circle(4);

        $result = $this->geometryCalculator->surfaceSumForTwoGeometryShapes($a, $b);

        $this->assertSame(12.56, $a->getSurface());
        $this->assertSame(50.24, $b->getSurface());
        $this->assertSame(62.8, $result);
    }

    /** @test */
    public function surfaceSumForTwoGeometryShapesShouldReturnCorrectSumForTriangles(): void
    {
        $a = new Triangle(2, 3, 4);
        $b = new Triangle(3, 4, 5);

        $result = $this->geometryCalculator->surfaceSumForTwoGeometryShapes($a, $b);

        $this->assertEquals(2.9, $a->getSurface(), '', 0.01);
        $this->assertSame(6.0, $b->getSurface());
        $this->assertEquals(8.9, $result, '', 0.01);
    }

    /** @test */
    public function surfaceSumForTwoGeometryShapesShouldReturnCorrectSumForCircleAndTriangle(): void
    {
        $a = new Circle(2);
        $b = new Triangle(3, 4, 5);

        $result = $this->geometryCalculator->surfaceSumForTwoGeometryShapes($a, $b);

        $this->assertSame(12.56, $a->getSurface());
        $this->assertSame(6.0, $b->getSurface());
        $this->assertSame(18.56, $result);
    }

    /** @test */
    public function circumferenceSumForTwoGeometryShapesShouldReturnCorrectSumForCircles(): void
    {
        $a = new Circle(2);
        $b = new Circle(4);

        $result = $this->geometryCalculator->circumferenceSumForTwoGeometryShapes($a, $b);

        $this->assertSame(12.56, $a->getCircumference());
        $this->assertSame(25.12, $b->getCircumference());
        $this->assertSame(37.68, $result);
    }

    /** @test */
    public function circumferenceSumForTwoGeometryShapesShouldReturnCorrectSumForTriangles(): void
    {
        $a = new Triangle(2, 3, 4);
        $b = new Triangle(3, 4, 5);

        $result = $this->geometryCalculator->circumferenceSumForTwoGeometryShapes($a, $b);

        $this->assertSame(9.0, $a->getCircumference());
        $this->assertSame(12.0, $b->getCircumference());
        $this->assertSame(21.0, $result);
    }

    /** @test */
    public function circumferenceSumForTwoGeometryShapesShouldReturnCorrectSumForCircleAndTriangle(): void
    {
        $a = new Circle(2);
        $b = new Triangle(3, 4, 5);

        $result = $this->geometryCalculator->circumferenceSumForTwoGeometryShapes($a, $b);

        $this->assertSame(12.56, $a->getCircumference());
        $this->assertSame(12.0, $b->getCircumference());
        $this->assertSame(24.56, $result);
    }
}
