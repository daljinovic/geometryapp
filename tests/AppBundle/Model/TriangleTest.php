<?php

namespace Tests\AppBundle\Model;

use AppBundle\Model\Triangle;
use PHPUnit\Framework\TestCase;

class TriangleTest extends TestCase
{
    /**
     * @test
     * @testWith [0, 0, 0]
     *           [-2, 3, 4]
     *           [2, -3, 4]
     *           [2, 3, -4]
     */
    public function triangleConstructionShouldThrowExceptionIfAnySideZeroOrLess(float $a, float $b, float $c): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage(sprintf('Stranice a=%f, b=%f, c=%f ne čine trokut.', $a, $b, $c));

        $triangle = new Triangle($a, $b, $c);
    }

    /**
     * @test
     * @testWith [2, 3, 6]
     *           [2, 6, 4]
     *           [7, 3, 4]
     */
    public function triangleConstructionShouldThrowExceptionIfAnyTwoSidesAreLessOrEqualThirdSide(
        float $a,
        float $b,
        float $c
    ): void {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage(sprintf('Stranice a=%f, b=%f, c=%f ne čine trokut.', $a, $b, $c));

        $triangle = new Triangle($a, $b, $c);
    }

    /** @test */
    public function getTypeShouldReturnTriangle(): void
    {
        $triangle = new Triangle(2, 3, 4);
        $this->assertSame('triangle', $triangle->getType());
    }

    /** @test */
    public function sideGettersShouldReturnSidesInDecimalFormat(): void
    {
        $triangle = new Triangle(2, 3, 4);
        $this->assertSame(2.0, $triangle->getA());
        $this->assertSame(3.0, $triangle->getB());
        $this->assertSame(4.0, $triangle->getC());
    }

    /** @test */
    public function getSurfaceShouldReturnCorrectlyCalculatedSurface(): void
    {
        $triangle = new Triangle(2, 3, 4);
        $this->assertEquals(2.904, $triangle->getSurface(), '', 0.001);
    }

    /** @test */
    public function getCircumferenceShouldReturnCorrectlyCalculatedCircumference(): void
    {
        $triangle = new Triangle(2, 3, 4);
        $this->assertSame(9.0, $triangle->getCircumference());
    }
}
