<?php

namespace Tests\AppBundle\Model;

use AppBundle\Model\Circle;
use PHPUnit\Framework\TestCase;

class CircleTest extends TestCase
{
    /**
     * @test
     * @testWith [0.0]
     *           [-2.0]
     */
    public function circleConstructionShouldThrowExceptionIfRadiusZeroOrLess(float $radius): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage(sprintf('Radijus "%f" mora biti pozitivan broj veći od 0.', $radius));

        $circle = new Circle($radius);
    }

    /** @test */
    public function getTypeShouldReturnCircle(): void
    {
        $circle = new Circle(2);
        $this->assertSame('circle', $circle->getType());
    }

    /** @test */
    public function getRadiusShouldReturnRadiusInDecimalFormat(): void
    {
        $circle = new Circle(2);
        $this->assertSame(2.0, $circle->getRadius());
    }

    /** @test */
    public function getSurfaceShouldReturnCorrectlyCalculatedSurface(): void
    {
        $circle = new Circle(2);
        $this->assertSame(12.56, $circle->getSurface());
    }

    /** @test */
    public function getCircumferenceShouldReturnCorrectlyCalculatedCircumference(): void
    {
        $circle = new Circle(2);
        $this->assertSame(12.56, $circle->getCircumference());
    }
}
