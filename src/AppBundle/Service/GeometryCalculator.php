<?php

namespace AppBundle\Service;

use AppBundle\Model\GeometryShape;

class GeometryCalculator
{
    /**
     * Zbraja površine dva dana geometrijska lika.
     */
    public function surfaceSumForTwoGeometryShapes(GeometryShape $a, GeometryShape $b): float
    {
        return $a->getSurface() + $b->getSurface();
    }

    /**
     * Zbraja opsege dva dana geometrijska lika.
     */
    public function circumferenceSumForTwoGeometryShapes(GeometryShape $a, GeometryShape $b): float
    {
        return $a->getCircumference() + $b->getCircumference();
    }
}
