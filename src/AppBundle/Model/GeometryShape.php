<?php

namespace AppBundle\Model;

abstract class GeometryShape
{
    protected const CIRCLE = 'circle';
    protected const TRIANGLE = 'triangle';

    /**
     * @var string
     */
    protected $type = '';

    /**
     * @var float
     */
    protected $surface = 0.0;

    /**
     * @var float
     */
    protected $circumference = 0.0;

    public function getType(): string
    {
        return $this->type;
    }

    public function getSurface(): float
    {
        return $this->surface;
    }

    public function getCircumference(): float
    {
        return $this->circumference;
    }

    /**
     * Površina geometrijskog lika.
     */
    protected abstract function calculateSurface(): float;

    /**
     * Opseg geometrijskog lika.
     */
    protected abstract function calculateCircumference(): float;
}
