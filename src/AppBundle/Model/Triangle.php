<?php

namespace AppBundle\Model;

use AppBundle\Model\GeometryShape;

class Triangle extends GeometryShape
{
    /**
     * @var float
     */
    private $a = 0.0;

    /**
     * @var float
     */
    private $b = 0.0;

    /**
     * @var float
     */
    private $c = 0.0;

    public function __construct(float $a, float $b, float $c)
    {
        if (false === $this->isValidTriangle($a, $b, $c)) {
            throw new \InvalidArgumentException(sprintf('Stranice a=%f, b=%f, c=%f ne čine trokut.', $a, $b, $c));
        }

        $this->a = $a;
        $this->b = $b;
        $this->c = $c;
        $this->type = self::TRIANGLE;
        $this->surface = $this->calculateSurface();
        $this->circumference = $this->calculateCircumference();
    }

    public function getA(): float
    {
        return $this->a;
    }

    public function getB(): float
    {
        return $this->b;
    }

    public function getC(): float
    {
        return $this->c;
    }

    /**
     * Površina trokuta putem formule "sqrt(s*(s-a)*(s-b)*(s-c))" gdje je "s" polovina njegova opsega.
     */
    protected function calculateSurface(): float
    {
        $s = $this->calculateCircumference() / 2;
        $surface = sqrt($s * ($s - $this->a) * ($s - $this->b) * ($s - $this->c));

        return $surface;
    }

    /**
     * Opseg trokuta zbroj je njegovih stranica.
     */
    protected function calculateCircumference(): float
    {
        return $this->a + $this->b + $this->c;
    }

    /**
     * Provjerava je li stranice zaista čine trokut.
     */
    private function isValidTriangle(float $a, float $b, float $c): bool
    {
        return $a > 0 && $b > 0 && $c > 0
            && ($a + $b > $c)
            && ($a + $c > $b)
            && ($b + $c > $a);
    }
}
