<?php

namespace AppBundle\Model;

use AppBundle\Model\GeometryShape;

class Circle extends GeometryShape
{
    private const PI = 3.14;

    /**
     * @var float
     */
    private $radius = 0.0;

    public function __construct(float $radius)
    {
        if ($radius <= 0) {
            throw new \InvalidArgumentException(sprintf('Radijus "%f" mora biti pozitivan broj veći od 0.', $radius));
        }

        $this->radius = $radius;
        $this->type = self::CIRCLE;
        $this->surface = $this->calculateSurface();
        $this->circumference = $this->calculateCircumference();
    }

    public function getRadius(): float
    {
        return $this->radius;
    }

    /**
     * Površina kruga putem formule "r^2*pi".
     */
    protected function calculateSurface(): float
    {
        return pow($this->radius, 2) * self::PI;
    }

    /**
     * Opseg kruga putem formule "2*r*pi".
     */
    protected function calculateCircumference(): float
    {
        return 2 * $this->radius * self::PI;
    }
}
