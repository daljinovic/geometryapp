<?php

namespace AppBundle\Controller;

use AppBundle\Model\Circle;
use AppBundle\Model\GeometryShape;
use AppBundle\Model\Triangle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class GeometryController extends Controller
{
    /**
     * Dohvaća podatke kruga za dani radijus.
     *
     * @Route("/circle/{radius}", name="CircleData", methods={"GET"})
     */
    public function circleAction(float $radius): JsonResponse
    {
        $circle = new Circle($radius);

        $jsonContent = $this->serializeGeometryShape($circle);

        return JsonResponse::fromJsonString($jsonContent);
    }

    /**
     * Dohvaća podatke trokuta za dane stranice.
     *
     * @Route("/triangle/{a}/{b}/{c}", name="TriangleData", methods={"GET"})
     */
    public function triangleAction(float $a, float $b, float $c): JsonResponse
    {
        $triangle = new Triangle($a, $b, $c);

        $jsonContent = $this->serializeGeometryShape($triangle);

        return JsonResponse::fromJsonString($jsonContent);
    }

    /**
     * Test servisa GeometryCalculator.
     *
     * @Route("/test", name="GeometryCalculatorTest")
     */
    public function testAction(): JsonResponse
    {
        $circle = new Circle(2);
        $triangle = new Triangle(3, 4, 5);

        $surfaceSum = $this->get('app.geometry_calculator')->surfaceSumForTwoGeometryShapes($circle, $circle);
        $circumferenceSum = $this->get('app.geometry_calculator')->circumferenceSumForTwoGeometryShapes($circle, $triangle);

        return new JsonResponse([
            'surface_sum' => $surfaceSum,
            'circumference_sum' => $circumferenceSum,
        ]);
    }

    /**
     * Serijalizira geometrijski lik u JSON string.
     */
    private function serializeGeometryShape(GeometryShape $shape): string
    {
        return $this->get('serializer')->serialize($shape, 'json');
    }
}
